var gulp = require('gulp');
var sass = require('gulp-sass');
var slim = require("gulp-slim");

gulp.task('styles', function() {
    gulp.src('scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('build/'))
});

gulp.task('slim', function(){
  gulp.src("*.slim")
    .pipe(slim({
      pretty: true
    }))
    .pipe(gulp.dest("build/"));
});

//Watch task
gulp.task('default',function() {
    gulp.watch('scss/*.scss',['styles']);
    gulp.watch('*.slim',['slim']);
});
