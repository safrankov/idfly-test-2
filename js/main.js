function save(){

    var pageData = {
        title: document.getElementById("title").value,
        text: document.getElementById("text").value,
        project: document.getElementById("project").value,
        urgency: document.querySelector('input[name="urgency"]:checked').value,
        date: document.getElementById("date").value
    }

    if (localStorage.getItem('key')){
        var arrayResult = JSON.parse(localStorage.getItem('key'));
    }
    else {
        var arrayResult = []
    }

    arrayResult.push(pageData);

    localStorage.setItem('key', JSON.stringify(arrayResult));

    document.querySelector('form').reset();
    show();
}

function show(){
    var savedData = JSON.parse(localStorage.getItem('key'));
    var htmlResult = '';

    if (!savedData){
        return
    }

    for (var i = 0; i < savedData.length; i++) {
        htmlResult = htmlResult + "<tr><td>" + savedData[i].title + "</td>";
        htmlResult = htmlResult + "<td>" + savedData[i].text + "</td>";
        htmlResult = htmlResult + "<td>" + savedData[i].project + "</td>";
        htmlResult = htmlResult + "<td>" + savedData[i].urgency + "</td>";
        htmlResult = htmlResult + "<td>" + savedData[i].date + "</td></tr>";
    }

    htmlResult = "<table class='table'><tr class='table-head'><td>Заголовок</td><td>Текст</td><td>Проект</td><td>Важность</td><td>Дата</td></tr>" + htmlResult + "</table>";

    document.querySelector('#result').innerHTML = htmlResult;
}

show();
